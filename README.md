## NodeJS Programming Task

NodeJS is Loncel's main programming language in the cloud space. It is quite important
that you feel comfortable with it. Hence this test.

*Note: This task should take no longer than 1-2 hours at the most.*


### Prerequisites

- Please note that this will require some basic [JavaScript](http://www.codecademy.com/tracks/javascript) and [ExpressJS](http://expressjs.com/) knowledge.

- You will need to have [NodeJS](http://www.nodejs.org/) installed to complete this task.

## Task

1. Fork this repository (if you don't know how to do that, Google is your friend)
2. Create a *source* folder to contain your code.
3. In the *source* directory, please create an ExpressJS app that accomplishes the following:
	- Connect to the [Github API](http://developer.github.com/)
	- Find the [nodejs](https://github.com/nodejs/node) repository
	- Find the most recent commits (choose at least 25 or more of the commits)
	- Create a route that displays the recent commits ordered by author.
	- If the commit hash ends in a number, color that row to red (#DC143C).

### Tests

Create the following unit test with the testing framework of your choice:

  1.  Verify that rows ending in a number are colored red.  

## Once Complete
1. Commit and Push your code to your new repository
2. Send us a pull request, we will review your code and get back to you
