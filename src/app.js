var express = require('express');
var app = express();

app.get('/', function (req, res) {
	  var xmlhttp = new XMLHttpRequest();
  var url = "https://api.github.com/repos/nodejs/node/commits";

  xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
          var commits = JSON.parse(xmlhttp.responseText);
          display(commits);
      }
  };

  xmlhttp.open("GET", url, true);
  xmlhttp.send();

  function display(commits) {

      commits.sort(function(a, b) {
          var nameA = a.commit.author.name.toLowerCase();
          var nameB = b.commit.author.name.toLowerCase();
          if (nameA < nameB) //sort string ascending
              return -1;
          if (nameA > nameB)
              return 1;
          return 0; //default return value (no sorting)
      })

      var out = "";

      var i;

      for (i = 0; i < commits.length; i++) {

          if (!isNaN(commits[i].sha.substr(commits[i].sha.length - 1, 1))) {
              out += '<div class="numeric" style="color:#DC143C">' + (i + 1) + '. ';
          } else {
              out += '<div>' + (i + 1) + '. ';
          }

          out += commits[i].sha + ': ' +
              commits[i].commit.author.name + '- ' +
              commits[i].commit.message +
              '</div>';
      }
      return out;
  }
});

app.listen(3000, function () {
	console.log('App to list commits on port 3000!');
});