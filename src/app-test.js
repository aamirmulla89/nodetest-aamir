var chaiExpect = require("chai").expect;
var commitLister = require("app");

describe("Commit lister", function() {
	it("Colors commits with sha ending in numeral red", function() {
		var xmlHttp = new XMLHttpRequest();
		xmlHttp.open("GET", 'http://localhost:3000', false);
		xmlHttp.send(null);
		var response = xmlHttp.responseText;
		var htmlPage = document.createElement('html');
		htmlPage.innerHtml = response;
		var numericCommits = document.getElementsByClassName('numeric');

		if (numericCommits) {
			numbericCommits.ForEach(function(commit) {
				expect(commit.style.color).to.equal("#DC143C");
			});
		}
	});
});